default: clean compile

clean:
	rm -rf app

compile:
	go build -o app

watch:
	go run main.go

