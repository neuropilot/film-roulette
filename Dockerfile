FROM golang

WORKDIR /go/src/app
COPY . .

RUN make clean compile

CMD ["./app"]