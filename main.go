package main

import (
	"encoding/json"
	"film-roulette/kinopoisk"
	"film-roulette/storage"
	"film-roulette/telegram"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/joho/godotenv"
	"log"
	"math/rand"
	"os"
	"strings"
)

func main() {
	err := godotenv.Load(".env")
	if err != nil {
		return
	}

	bot, err := tgbotapi.NewBotAPI(os.Getenv("TELEGRAM_BOT_TOKEN"))

	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := bot.GetUpdatesChan(u)

	for update := range updates {
		if isSupportedMessage(bot, update) {
			container := storage.Read()

			if update.Message != nil {
				text := strings.Replace(update.Message.Text, "@"+bot.Self.UserName, "", 1)
				text = strings.Trim(text, " ")
				switch text {
				case "/start":
					go greeting(bot, update)
				case "/roll":
					go roll(bot, update, container)
				case "/add":
					go startAdd(bot, update, container)
				case "/list":
					go list(bot, update, container)
				case "/delete":
					go prepareForDelete(bot, update, container)
				default:
					go save(bot, update, container)
				}
			}

			if update.CallbackQuery != nil {
				callbackData := telegram.CallbackData{}
				err = json.Unmarshal([]byte(update.CallbackQuery.Data), &callbackData)
				if err != nil {
					log.Println(err)
				}

				switch callbackData.Action {
				case "/delete":
					go doDelete(bot, update, container, callbackData.Payload.DeleteButtonPayload)
					break
				case "/add-by-id":
					go addById(bot, update, container, callbackData.Payload.AddByIdButtonPayload)
					break
				}
			}
		}

	}
}

func isSupportedMessage(bot *tgbotapi.BotAPI, update tgbotapi.Update) bool {
	return update.Message != nil &&
		(bot.IsMessageToMe(*update.Message) || update.FromChat().ID == update.Message.From.ID) ||
		update.CallbackQuery != nil
}

func greeting(bot *tgbotapi.BotAPI, update tgbotapi.Update) {
	response := telegram.Response{Bot: bot, To: update.Message.Chat.ID}
	response.Send("Это наш бот для выбора фильмов")
}

func roll(bot *tgbotapi.BotAPI, update tgbotapi.Update, container *storage.Container) {
	response := telegram.Response{Bot: bot, To: update.Message.Chat.ID}
	if len(container.Films) == 0 {
		response.SendListIsEmpty()
		return
	}

	index := rand.Intn(len(container.Films))
	response.SendFilm(container.Films[index])
}

func startAdd(bot *tgbotapi.BotAPI, update tgbotapi.Update, container *storage.Container) {
	container.Status = storage.StatusWaitingForAdd
	container.Buffer = storage.Film{}
	storage.Write(container)
	response := telegram.Response{Bot: bot, To: update.Message.Chat.ID}
	response.Send("Введите название фильма")
}

func save(bot *tgbotapi.BotAPI, update tgbotapi.Update, container *storage.Container) {
	response := telegram.Response{Bot: bot, To: update.Message.Chat.ID}
	if container.Status == storage.StatusWaitingForAdd {

		waitingMessage := response.Send("Минутку...")

		apiResponse, err := kinopoisk.GetFilmsByName(update.Message.Text)
		if err != nil {
			log.Println(err)
		}

		var film storage.Film
		if apiResponse.Total == 1 {

			filmData, err := kinopoisk.GetFilmById(apiResponse.Docs[0].Id)
			if err != nil {
				log.Println(err)
			}

			directorName := ""
			director, err := filmData.GetDirector()
			if err == nil {
				directorName = director.Name
			}

			film = storage.Film{
				Url:         kinopoisk.BuildUrl(filmData.FilmShortData),
				Name:        filmData.Name,
				Description: filmData.Description,
				Year:        filmData.Year,
				Director:    directorName,
			}

			container.Status = storage.StatusDefault
			container.Films = append(container.Films, film)
			storage.Write(container)

			response.Delete(&waitingMessage)
			response.SendSuccessfullyAdded(film)
			return
		}

		if apiResponse.Total == 0 || apiResponse.Total > kinopoisk.DefaultLimit {
			response.Delete(&waitingMessage)
			response.Send("Фильм не найден!")
			return
		}

		var buttons []tgbotapi.InlineKeyboardButton
		var variants []string
		for _, data := range apiResponse.Docs {

			filmData, err := kinopoisk.GetFilmById(data.Id)
			if err != nil {
				log.Println(err)
			}

			directorName := ""
			director, err := filmData.GetDirector()
			if err == nil {
				directorName = director.Name
			}

			clbData := telegram.CallbackData{
				Action: "/add-by-id",
				Payload: telegram.ButtonPayload{
					AddByIdButtonPayload: telegram.AddByIdButtonPayload{
						Id: data.Id,
					},
				},
			}

			film = storage.Film{
				Name:        data.Name,
				Url:         kinopoisk.BuildUrl(data),
				Description: data.Description,
				Year:        data.Year,
				Director:    directorName,
			}

			encoded := string(clbData.ToJson())

			buttons = append(buttons, tgbotapi.InlineKeyboardButton{
				Text:         film.GetNameWithYear(),
				CallbackData: &encoded,
			})

			variants = append(variants, film.GetFullName())
		}

		response.Delete(&waitingMessage)
		response.SendWithButtons("Выберите, какой фильм будем добавлять:\n\n"+strings.Join(variants, "\n"), buttons)
		return

	}

	response.Send("Введите какую-нибудь команду")
}

func list(bot *tgbotapi.BotAPI, update tgbotapi.Update, container *storage.Container) {
	var result []string
	for _, film := range container.Films {
		result = append(result, film.GetFullName())
	}

	response := telegram.Response{Bot: bot, To: update.Message.Chat.ID}
	if len(result) == 0 {
		response.SendListIsEmpty()
		return
	}

	response.Send(strings.Join(result, "\n"))
}

func prepareForDelete(bot *tgbotapi.BotAPI, update tgbotapi.Update, container *storage.Container) {

	response := telegram.Response{Bot: bot, To: update.Message.Chat.ID}
	if len(container.Films) == 0 {
		response.SendListIsEmpty()
		return
	}

	container.Status = storage.StatusWaitingForRemove
	storage.Write(container)

	var buttons []tgbotapi.InlineKeyboardButton
	for i, film := range container.Films {
		clbData := telegram.CallbackData{
			Action: "/delete",
			Payload: telegram.ButtonPayload{
				DeleteButtonPayload: telegram.DeleteButtonPayload{
					Index: i,
				},
			},
		}

		encoded := string(clbData.ToJson())

		buttons = append(buttons, tgbotapi.InlineKeyboardButton{
			Text:         film.GetNameWithYear(),
			CallbackData: &encoded,
		})
	}

	response.SendWithButtons("Выберите, какой фильм удалить из подборки:", buttons)
}

func doDelete(bot *tgbotapi.BotAPI, update tgbotapi.Update, container *storage.Container, payload telegram.DeleteButtonPayload) {

	film := container.Films[payload.Index]
	container.Films = append(container.Films[:payload.Index], container.Films[payload.Index+1:]...)
	container.Status = storage.StatusDefault
	storage.Write(container)

	response := telegram.Response{Bot: bot, To: update.CallbackQuery.Message.Chat.ID}
	response.Delete(update.CallbackQuery.Message)
	response.Send(fmt.Sprintf("Фильм %s удалён", film.GetLink()))
}

func addById(bot *tgbotapi.BotAPI, update tgbotapi.Update, container *storage.Container, payload telegram.AddByIdButtonPayload) {
	response := telegram.Response{Bot: bot, To: update.CallbackQuery.Message.Chat.ID}
	filmData, err := kinopoisk.GetFilmById(payload.Id)
	if err != nil {
		log.Println(err)
	}

	directorName := ""
	director, err := filmData.GetDirector()
	if err == nil {
		directorName = director.Name
	}

	container.Status = storage.StatusDefault
	film := storage.Film{
		Name:        filmData.Name,
		Url:         kinopoisk.BuildUrl(filmData.FilmShortData),
		Description: filmData.Description,
		Year:        filmData.Year,
		Director:    directorName,
	}
	container.Films = append(container.Films, film)

	storage.Write(container)

	response.Delete(update.CallbackQuery.Message)

	response.SendSuccessfullyAdded(film)
}
