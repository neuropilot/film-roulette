module film-roulette

go 1.17

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1
	github.com/joho/godotenv v1.4.0
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	golang.org/x/net v0.0.0-20220624214902-1bab6f366d9e // indirect
)
