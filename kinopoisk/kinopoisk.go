package kinopoisk

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strconv"
)

type Person struct {
	Id         int    `json:"id"`
	Name       string `json:"name"`
	Profession string `json:"enProfession"`
}

type FilmShortData struct {
	Id          int    `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Year        int    `json:"year"`
}

type FilmFullData struct {
	FilmShortData
	Persons []Person `json:"persons"`
}

type ApiResponse struct {
	Docs  []FilmShortData `json:"docs"`
	Total int             `json:"total"`
}

type FilmId int

const DefaultLimit = 10

func GetFilmsByName(name string) (ApiResponse, error) {

	params := "query=" + url.QueryEscape(name) + "&" +
		"page=1" + "&" +
		"limit=" + strconv.Itoa(DefaultLimit)

	requestUri := fmt.Sprintf("https://api.kinopoisk.dev/v1.4/movie/search?%s", params)

	client := &http.Client{}
	request, err := http.NewRequest("GET", requestUri, nil)
	if err != nil {
		panic("Invalid request")
	}

	request.Header.Set("X-API-KEY", os.Getenv("KINOPOISK_TOKEN"))

	response, err := client.Do(request)
	if err != nil {
		panic("No response from request")
	}
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)

	var result ApiResponse
	err = json.Unmarshal(body, &result)
	if err != nil {
		panic("Cannot parse response")
	}

	var filtered []FilmShortData
	for _, doc := range result.Docs {
		fmt.Println(doc.Name)
		if doc.Year != 0 {
			filtered = append(filtered, doc)
		}
	}
	result.Docs = filtered
	result.Total = len(filtered)

	return result, err

}

func GetFilmById(id int) (FilmFullData, error) {
	requestUri := fmt.Sprintf("https://api.kinopoisk.dev/v1.4/movie/%s", strconv.Itoa(id))

	client := &http.Client{}
	request, err := http.NewRequest("GET", requestUri, nil)
	if err != nil {
		panic("Invalid request")
	}
	request.Header.Set("X-API-KEY", os.Getenv("KINOPOISK_TOKEN"))

	response, err := client.Do(request)
	if err != nil {
		panic("No response from request")
	}
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)

	fmt.Println(string(body))

	var result FilmFullData
	err = json.Unmarshal(body, &result)
	if err != nil {
		panic("Cannot parse response")
	}

	return result, err
}

func BuildUrl(film FilmShortData) string {
	return fmt.Sprintf("https://www.kinopoisk.ru/film/%d", film.Id)
}

func (doc *FilmFullData) GetDirector() (*Person, error) {
	for _, person := range doc.Persons {
		if person.Profession == "director" {
			return &person, nil
		}
	}
	return nil, errors.New("director not found")
}
