package telegram

import (
	"encoding/json"
	"film-roulette/storage"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"log"
)

type DeleteButtonPayload struct {
	Index int
}

type AddByIdButtonPayload struct {
	Id int
}

type ButtonPayload struct {
	DeleteButtonPayload
	AddByIdButtonPayload
}

type CallbackData struct {
	Action  string
	Payload ButtonPayload
}

type Response struct {
	Bot *tgbotapi.BotAPI
	To  int64
}

func (response *Response) Send(text string) tgbotapi.Message {
	msg := tgbotapi.NewMessage(response.To, text)
	msg.ParseMode = tgbotapi.ModeMarkdown
	msg.DisableWebPagePreview = true

	instance, err := response.Bot.Send(msg)
	if err != nil {
		log.Println(err)
	}
	return instance
}

func (response *Response) SendFilm(film storage.Film) tgbotapi.Message {
	msg := tgbotapi.NewMessage(response.To, fmt.Sprintf("%s\n\n*Режиссёр*: %s\n\n%s", film.GetLink(), film.Director, film.Description))
	msg.ParseMode = tgbotapi.ModeMarkdown
	msg.DisableWebPagePreview = false

	instance, err := response.Bot.Send(msg)
	if err != nil {
		log.Println(err)
	}
	return instance
}

func (response *Response) SendListIsEmpty() tgbotapi.Message {
	return response.Send("Список пуст! Добавить фильм можно с помощью команды /add")
}

func (response *Response) SendWithButtons(text string, buttons []tgbotapi.InlineKeyboardButton) {

	var rows [][]tgbotapi.InlineKeyboardButton

	for _, rowButtonsData := range chunk(buttons, 2) {
		var btns []tgbotapi.InlineKeyboardButton
		for _, btn := range rowButtonsData {
			btns = append(btns, btn)
		}
		rows = append(rows, btns)
	}

	keyboard := tgbotapi.NewInlineKeyboardMarkup(rows...)

	msg := tgbotapi.NewMessage(response.To, text)
	msg.ParseMode = tgbotapi.ModeMarkdown
	msg.DisableWebPagePreview = true
	msg.ReplyMarkup = keyboard
	_, err := response.Bot.Send(msg)
	if err != nil {
		log.Println(err)
	}
}

func (response *Response) SendSuccessfullyAdded(film storage.Film) {
	text := fmt.Sprintf("Фильм успешно добавлен: %s", film.GetFullName())
	msg := tgbotapi.NewMessage(response.To, text)

	msg.ParseMode = tgbotapi.ModeMarkdown
	msg.DisableWebPagePreview = false

	response.Bot.Send(msg)
}

func (response *Response) Delete(message *tgbotapi.Message) {
	deleteMessage := tgbotapi.NewDeleteMessage(response.To, message.MessageID)
	_, err := response.Bot.Send(deleteMessage)
	if err != nil {
		log.Println(err)
	}
}

func (callbackData *CallbackData) ToJson() []byte {
	jsonStr, err := json.Marshal(callbackData)
	if err != nil {
		panic(err)
	}
	return jsonStr
}

func chunk(origin []tgbotapi.InlineKeyboardButton, size int) [][]tgbotapi.InlineKeyboardButton {
	var result [][]tgbotapi.InlineKeyboardButton
	for i := 0; i < len(origin); i += size {
		end := i + size

		if end > len(origin) {
			end = len(origin)
		}

		result = append(result, origin[i:end])
	}

	return result
}
