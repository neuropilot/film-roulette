package storage

import (
	"encoding/json"
	"fmt"
)

const (
	StatusDefault          = "default"
	StatusWaitingForAdd    = "waitingForAdd"
	StatusWaitingForRemove = "waitingForRemove"
)

type Film struct {
	Name        string `json:"name"`
	Url         string `json:"url"`
	Description string `json:"description"`
	Year        int    `json:"year"`
	Director    string `json:"director"`
}

type Container struct {
	Status string `json:"status"`
	Films  []Film `json:"films"`
	Buffer Film   `json:"buffer"`
}

func (container *Container) ToJson() []byte {
	jsonStr, err := json.Marshal(container)
	if err != nil {
		panic(err)
	}
	return jsonStr
}

func (film *Film) GetNameWithYear() string {
	return fmt.Sprintf("%s (%d)", film.Name, film.Year)
}

func (film *Film) GetLink() string {
	return fmt.Sprintf("[%s](%s)", film.GetNameWithYear(), film.Url)
}

func (film *Film) GetFullName() string {
	return fmt.Sprintf("%s (%s)", film.GetLink(), film.Director)
}
