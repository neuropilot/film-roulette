package storage

import (
	"encoding/json"
	"os"
)

const RelatedFilename = "/resource/storage.json"
const DefaultPerm = 7777

func Read() (container *Container) {
	pwd, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	filename := pwd + RelatedFilename

	bytes, err := os.ReadFile(filename)
	if err != nil {
		_, err = os.Create(filename)
		if err != nil {
			panic(err)
		}

		defaultStorage := Container{
			Status: StatusDefault,
			Films:  []Film{},
		}
		err = os.WriteFile(filename, defaultStorage.ToJson(), DefaultPerm)

		if err != nil {
			panic(err)
		}
		bytes, err = os.ReadFile(filename)
	}

	err = json.Unmarshal(bytes, &container)
	if err != nil {
		panic(err)
	}
	return
}

func Write(container *Container) {
	pwd, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	filename := pwd + RelatedFilename
	err = os.WriteFile(filename, container.ToJson(), DefaultPerm)
	if err != nil {
		panic(err)
	}
}
